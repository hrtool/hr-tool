var main = function(){

  $("#tab-container ul > li").click(function(){

    $(this).addClass("active-vert-tab");
      $(this).siblings().removeClass("active-vert-tab");
  });

  $("#tab-container ul li").click(function(){
                 switch($(this).index()){
                     case 0 :
                              $("div.views > div#view1").removeClass("hidden");
                              $("div.views > div#view1").siblings().addClass("hidden");
                              break;

                     case 1 :
                              $("div.views > div#view2").removeClass("hidden");
                              $("div.views > div#view2").siblings().addClass("hidden");
                              break;

                     case 2:
                              $("div.views > div#view3").removeClass("hidden");
                              $("div.views > div#view3").siblings().addClass("hidden");
                              break;

                     case 3:
                              $("div.views > div#view4").removeClass("hidden");
                              $("div.views > div#view4").siblings().addClass("hidden");
                              break;

                     case 4:
                              $("div.views > div#view5").removeClass("hidden");
                              $("div.views > div#view5").siblings().addClass("hidden");
                              break;

                     default: break;
                 }
             });

   $(function() {
       $('#side-menu').metisMenu();
   });

  var x = true;;
  $("#navbar-button").click(function(){
    if(x){
    $(".mobile-menus").animate({left: "0"});
    }
    else{
      $(".mobile-menus").animate({left: "-81%"});
    }
    x=!x;
  });

   $(".nav-second-level li").click(function(){
     var tab_id = $(this).attr('data-tab');
    $(".universal").hide();
    $("#"+tab_id).show();
    var str = tab_id.substr(0,5);
    $("#"+str).removeClass("hidden");
    $("#"+str).siblings().addClass("hidden");

  });

  $(document).on("click","#corr-addchk",function(){
    if($(this).prop("checked") == true)
      {
          $("#corr-add").hide();
      }
      else if($(this).prop("checked") == false)
      {
          $("#corr-add").show();
      }
  });

  $('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
    $(this).siblings().removeClass('active');
    $(this).removeClass('active');
//		$("#"+tab_id).removeClass('current');
		$("#"+tab_id).siblings().removeClass('current');

		$(this).addClass('current active');
		$("#"+tab_id).addClass('current');
    if($(this).parents().hasClass("submenu")){
      //$("#"+tab_id).siblings().hide();
      //$("#"+tab_id).show();
      $(this).removeClass("active");
    }



    //   if($(this).hasClass("submenu")){
    //   $(this).siblings().removeClass("current");
    //   $(this).siblings().removeClass("active");
    //   $(this).addClass("current");
    //   // $(this).siblings().addClass("current");
    //   // $(".submenu").children().removeClass("active");
    // };

	})


function appendElements(key,pos){
  switch (key) {
    case "dependent":
                        var string = '<div class="added-elements clearfix"><div class="row"><div class="col-md-3"><div class="form-group has-feedback"><label>Name</label><input type="text" class="form-control " name="dependent-name[]" id="inputValidation" placeholder="" /></div></div><div class="col-md-4"><div class="form-group has-feedback"><label>Relationship</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-3"><div class="form-group has-feedback"><label>Date of Birth</label><span class="form-control-feedback"><i class="fa fa-calendar"></i></span><input type="text" class="form-control datepicker"/></div></div><div class="col-md-2 delete-ele"><span class="class2 pull-right"><i class="fa fa-minus"></i></span></div></div></div>'
                        $(".dependent").parent(".row").parent(".added-elements").parent(".expansion").append(string);
                        break;

    case "emergency" :
                        var string = '<div class="added-elements clearfix"><div class="row"><div class="col-md-2"><div class="form-group has-feedback"><label>Name</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-2"><div class="form-group has-feedback"><label>Relationship</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-2"><div class="form-group has-feedback"><label>Mobile</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-2"><div class="form-group has-feedback"><label>Home Telephone</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-2"><div class="form-group has-feedback"><label>Work Telephone</label><input type="text" class="form-control" id="inputValidation" placeholder="" /></div></div><div class="col-md-2 delete-ele"><span class="class2  pull-right"><i class="fa fa-minus"></i></span></div></div></div>';
                        $(".emergency").parent(".row").parent(".added-elements").parent(".expansion").append(string);
                        break;

   case "file-upload" :
                       var string  = '<div class="row"><div class="col-md-5"><div class="form-group has-feedback"><label>Upload Photo</label><input type="file" class="form-control" id="" placeholder="" /></div></div><div class="col-md-5"><div class="form-group has-feedback"><label>comment</label><input type="text" class="form-control" id="" placeholder="" /></div></div><div class="col-md-2 delete-upload"><span class="class2  pull-right"><i class="fa fa-minus"></i></span></div></div>';
                       $(pos).parent(".row").after(string);
                       break;
    default:
                        break;

  }
}

$(".add-ele").click(function(){
  if($(this).hasClass("dependent"))
      appendElements("dependent");
  if($(this).hasClass("emergency"))
      appendElements("emergency");
  if($(this).hasClass("add-photos"))
      appendElements("file-upload",this);
});


  $(".refine-search").click(function(){
    $('.refineVal').toggle();
    $('.class3').toggle();
    $('.class4').toggle();
  });

  $(".expand").on("click",function(){
    $(this).siblings(".expansion").toggle();
    $(this).children('span').toggle();
  });

  $(document).on("click",".delete-ele",function(){
    $(this).parent(".row").parent(".added-elements").remove();
  });
  //delete-upload
  $(document).on("click",".delete-upload",function(){
    $(this).parent(".row").remove();
  });

  // $("#apply-leave").click(function(){
  //   $("#view2 *").prop("disabled","true");
  //   $("li").addClass("disabled");
  //   $("li").off("click");
  //   $("#leave-application *").removeAttr( "disabled");
  //   $("#leave-application").toggle();
  // });

  // $("#admin-leave-entitlements button").click(function(){
  //   $("#view22 *").prop("disabled","true");
  //   $("#assign-ent-app *").removeAttr("disabled");
  //   $("#assign-ent-app").toggle();
  // });



  // $("#entitlement-hide").click(function(){
  //   $(this).closest(".application").hide();
  //   $("#view22 *" ).removeAttr("disabled");
  // });


  $("#add-leave").click(function(){
    $("#add-leave-app").toggle();
  });


  $(".navbar li.disabled a").click(function() {
     return false;
   });
   var myFunc = function(event){
     //event.stopPropagation();
     console.log("abc");
}

// $('#web').on('click', myFunc); //bind myFunc
// $('#web').off('click'); // click is succesfully removed
// $('#web').on('click', myFunc);

  // $("#leave-apply button").click(function(){
  //
  //   $(this).closest(".application").hide();
  //   $("#view2 *" ).removeAttr("disabled");
  //   $("li").removeClass("disabled");
  //   $("body").on('click',main);
  //   //console.log($("li").bind("click"));
  // });
  //
  // $("#myasset-apply").click(function(){
  //   $(this).closest(".application").hide();
  //   $("#view5 *" ).removeAttr("disabled");
  // });

  $(".show-application").click(function(){
    $(this).closest(".request-filter").children(".application").show();
    //$("li").unbind("click");
//    $('#configform')[0].reset();
//    $(this).closest(".request-filter").children(".application").children("form").reset();
    $(this).closest(".universal").find("input, select, button").prop("disabled","true");
    $(".application *").removeAttr("disabled");
  });

  function closeApp(pos){
    $(pos).closest(".application").hide();
    //$("li").bind("click",main);
    $(pos).closest("form")[0].reset();
    $("input, select, button").removeAttr("disabled");
  }

  $(".hide-app").click(function(){
    closeApp(this);
  });

  // $("#apply-asset").click(function(){
  //   $("#view5 *").prop("disabled","true");
  //   $("#request-asset-application *").removeAttr("disabled");
  //   $("#request-asset-application").toggle();
  // });



  // $(document).on("focus",".datepicker",function(){
  //     $(this).datepicker();
  // });


  // $('.select').focus(function(){
  //   var x = $(this).width();
  //   x=x+58;
  //   x=x+"px";
  //   $(this).siblings('.select_list').css("width",x);
  //   $(this).siblings('.select_list').css("display","block");
  // });

  document.getElementById('getImage').onclick = function() {
      document.getElementById('my_file').click();
  }

  $(".editAction").click(function(){
    $(this).closest('table').siblings('.result-container').css("display","block");
    $(this).closest('table').siblings('.result-container').css("z-index","100");
    $(this).closest('table').siblings('.result-container').css("background-color","#000");
   });

  $(".closeResult").click(function(){
     $(this).closest('.result-container').css("display","none");
     $(this).closest('.result-container').css("z-index","-1");
     $(this).closest('.result-container').css("background-color","#fff");
  });

  $(function () {

    $(".from").datepicker({
        minDate : 0,
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate());
            $(this).closest(".fromdate").siblings(".todate").children().children(".to").datepicker("option", "minDate", dt);
        }
    });
    $(".to").datepicker({
        minDate : 0,
        numberOfMonths: 1,
        onSelect: function (selected) {
            var dt = new Date(selected);
            dt.setDate(dt.getDate());
            $(this).closest(".todate").siblings(".fromdate").children().children(".from").datepicker("option", "maxDate", dt);
        }
    });
  });

  var numberVal = function(num)
    {
        var numpat = /^[0-9]+$/;
        if(numpat.test(num))
            return true;
        else
            return false;
    }

    $(".number").focusout(function(){
        var num = $('.number').val().trim();
          if(!numberVal(num))
          {
            if($(this).parent().children("p").length == 0)
              $(this).parent().append("<p class='error-msg'>Number Only...</p>");
          }
          else
              $(this).parent().children().remove("p");
    });
    $(".mandatory").focusout(function(){
        var text = $(this).val().trim();
          if(text == "")
          {
            if($(this).parent().children("p").length == 0)
              $(this).parent().append("<p class='error-msg'>Comp...</p>");
          }
          else
              $(this).parent().children().remove("p");
    });

    var mandatoy_fields = function(elem){
        var text = $(elem).val();
            text = text.trim();
            if( text == "" )
            {
                var count = $(elem).closest('.form-group').children('p').length;
                if(count == 0)
                    $(elem).parent().append("<p class='error-msg'>comp...</p>");
            }
            else{
                  $(elem).parent().children().remove("p");
                }
    };

    $(".formSubmit").submit(function(event){
      $.each($(".mandatory"),function(){
        mandatoy_fields(this);
      });
      var err = $(this).find(".error-msg").length;

      if(err != 0){
        event.preventDefault();
      }
      else{
        closeApp(this);
      }
     });


};

$(document).ready(main);
